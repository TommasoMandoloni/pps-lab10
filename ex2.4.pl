average(L, A) :- average(L, 0, 0, A).
average([], C, S, A) :- A is S/C.
average([H|T], C, S, A) :-
	Ctmp is C+1,
	Stmp is S+H,
	average(T, Ctmp, Stmp, A).