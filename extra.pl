last([],N,[N]).
last([H|T],N,[H|R]) :- last(T,N,R).

inv([H],[H]).
inv([H|T],R) :- inv(T,RevT), last(RevT,H,R).

double(L1,L2) :- append(L1,L1,L2).

times(L1,1,L2) :- append(L1,[],L2).
times(L1,2,L2) :- double(L1,L2).
times(L1,N,R) :- N > 1, Tmp is N - 1, times(L1,Tmp,L2), append(L1,L2,R).

proj([],[]).
proj([[First|_]|X], [First|Y]) :- proj(X,Y).