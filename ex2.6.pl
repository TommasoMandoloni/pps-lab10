max([M], M).
max([H|T], M) :- max(T, M), M >= H.
max([H|T], H) :- max(T, M), M < H.

min([M], M).
min([H|T], M) :- min(T, M), M =< H.
min([H|T], H) :- min(T, M), M > H.

max(List, Max, Min) :- max(List, Max), min(List, Min).