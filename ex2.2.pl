size([], zero).
size([_|T], s(N)) :- size(T, N).