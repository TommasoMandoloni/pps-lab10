last([],N,[N]).
last([H|T],N,[H|R]) :- last(T,N,R).

seqR2(0,[0]).
seqR2(N,L) :- N > 0, Tmp is N - 1, seqR2(Tmp,T), last(T,N,L).