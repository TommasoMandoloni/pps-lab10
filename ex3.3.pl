search(X,[X|_]).
search(X,[_|T]) :- search(X,T).

sublist([H],L2) :- search(H,L2).
sublist([H|T],L2) :- search(H,L2), sublist(T,L2). 