all_bigger([A],[B]) :- A > B.
all_bigger([A|X],[B|Y]) :- all_bigger(X,Y), A > B.