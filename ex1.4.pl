search(E, [E|_]).
search(E, [_|T]) :- search(E, T).

search_anytwo(E, [E|T]) :- search(E, T).
search_anytwo(E, [_|T]) :- search_anytwo(E, T).