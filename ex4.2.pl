seqR(0,[0]).
seqR(N,[N|T]) :- N > 0, TMP is N - 1, seqR(TMP,T).