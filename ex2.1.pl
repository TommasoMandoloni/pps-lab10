size([], 0).
size([_|T], M) :- size(T,N), M is N+1.